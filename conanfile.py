import shutil

from conans import CMake, ConanFile, tools


class SimdJsonConan(ConanFile):
    name        = "simdjson"
    version     = "0.6.0"
    license     = "MIT"
    author      = "toge.mail@gmail.com"
    url         = "https://bitbucket.org/toge/conan-simdjson/"
    description = "Parsing gigabytes of JSON per second"
    topics      = ("json", "simd")
    settings    = "os", "compiler", "build_type", "arch"
    generators = "cmake"
    options = {
        "haswell"   : [True, False],
        "westmere"  : [True, False],
        "arm64"     : [True, False],
        "fallback"  : [True, False],
        "static"    : [True, False],
        "sanitize"  : [True, False],
        "threads"   : [True, False],
        "exceptions": [True, False],
    }
    default_options = {
        "haswell"   : True,
        "westmere"  : True,
        "arm64"     : True,
        "fallback"  : True,
        "static"    : True,
        "sanitize"  : False,
        "threads"   : True,
        "exceptions": True,
    }

    @property
    def _supported_cppstd(self):
        return ["14", "gnu14", "17", "gnu17", "20", "gnu20"]

    def _has_support_for_cpp14(self):
        supported_compilers = [['gcc', 7], ["clang", 6], ["apple-clang", 10], ["Visual Studio", 14.3]]
        compiler, version = self.settings.compiler, tools.Version(self.settings.compiler.version)
        return any(compiler == sc[0] and version >= sc[1] for sc in supported_compilers)

    def configure(self):
        if self.settings.compiler.cppstd and not self.settings.compiler.cppstd in self._supported_cppstd:
            raise ConanInvalidConfiguration("This library requires c++14 or higher. {} required.".format(self.settings.compiler.cppstd))
        if not self._has_support_for_cpp14():
            raise ConanInvalidConfiguration("This library requires c++14 or higher support standard. {} {} is not supported.".format(self.settings.compiler, self.settings.compiler.version))

    def source(self):
        tools.get("https://github.com/lemire/simdjson/archive/v{}.zip".format(self.version))
        shutil.move("simdjson-{}".format(self.version), "simdjson")

    def build(self):
        cmake = CMake(self)
        cmake.definitions["SIMDJSON_IMPLEMENTATION_HASWELL"]  = self.options.haswell
        cmake.definitions["SIMDJSON_IMPLEMENTATION_WESTMERE"] = self.options.westmere
        cmake.definitions["SIMDJSON_IMPLEMENTATION_ARM64"]    = self.options.arm64
        cmake.definitions["SIMDJSON_IMPLEMENTATION_FALLBACK"] = self.options.fallback
        cmake.definitions["SIMDJSON_BUILD_STATIC"]            = self.options.static
        cmake.definitions["SIMDJSON_SANITIZE"]                = self.options.sanitize
        cmake.definitions["SIMDJSON_EXCEPTIONS"]              = self.options.exceptions
        cmake.definitions["SIMDJSON_JUST_LIBRARY"]            = True
        cmake.configure(source_dir = "simdjson")
        cmake.build(target="simdjson")

    def package(self):
        self.copy("*.h",     dst="include", src="simdjson/include")
        self.copy("*.lib",   dst="lib",     keep_path=False)
        self.copy("*.dll",   dst="bin",     keep_path=False)
        self.copy("*.so",    dst="lib",     keep_path=False)
        self.copy("*.dylib", dst="lib",     keep_path=False)
        self.copy("*.a",     dst="lib",     keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["simdjson"]
