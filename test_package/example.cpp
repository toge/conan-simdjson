#include <iostream>

#include <simdjson.h>

int main() {
    simdjson::dom::parser parser;
    auto&& [doc, err] = parser.parse("[1,2,3]"_padded);  

    if (err) {
        std::cerr << "parse error!" << std::endl;
        return 1;
    }

    std::cout << doc.type() << std::endl;

    return 0;
}
